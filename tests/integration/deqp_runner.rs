use super::*;
use ::deqp_runner::{RunnerResults, RunnerStatus};
use anyhow::{Context, Result};
use std::io::Cursor;
use std::process::{Command, Stdio};

/// Builder for a mocked deqp-runner invocation
#[derive(Default)]
struct DeqpMock {
    baseline: Vec<String>,
    skips: Vec<String>,
    flakes: Vec<String>,
    runner_args: Vec<String>,
}

impl DeqpMock {
    pub fn new() -> DeqpMock {
        Default::default()
    }

    pub fn run<S: AsRef<str>>(&self, tests: Vec<S>) -> Result<RunnerCommandResult> {
        let caselist = lines_tempfile(&tests).context("caselist file")?;

        let output_dir = tempfile::tempdir().context("Creating output dir")?;

        // Get the location of our deqp-runner binary from rustc
        let deqp_runner = env!("CARGO_BIN_EXE_deqp-runner");

        let mut cmd = Command::new(&deqp_runner);
        let child = cmd.stdout(Stdio::piped()).stderr(Stdio::piped());
        let child = child.arg("run");

        let child = child.arg("--deqp");
        let child = child.arg(&deqp_runner);

        let child = child.arg("--output");
        let child = child.arg(output_dir.path());

        let child = child.arg("--caselist");
        let child = child.arg(&caselist);

        let child = child.arg("--timeout");
        let child = child.arg("1");

        #[allow(unused_variables)]
        let baseline_file = if !self.baseline.is_empty() {
            let baseline_file = lines_tempfile(&self.baseline).context("baseline file")?;

            child.arg("--baseline");
            child.arg(&baseline_file);

            Some(baseline_file)
        } else {
            None
        };

        #[allow(unused_variables)]
        let skips_file = if !self.skips.is_empty() {
            let skips_file = lines_tempfile(&self.skips).context("skips file")?;

            child.arg("--skips");
            child.arg(&skips_file);

            Some(skips_file)
        } else {
            None
        };

        #[allow(unused_variables)]
        let flakes_file = if !self.flakes.is_empty() {
            let flakes_file = lines_tempfile(&self.flakes).context("flakes file")?;

            child.arg("--flakes");
            child.arg(&flakes_file);

            Some(flakes_file)
        } else {
            None
        };

        for arg in &self.runner_args {
            child.arg(arg);
        }

        child.arg("--");
        child.arg("mock-deqp"); // Passed as the first arg of the "deqp" binary (the deqp-runner we passed as --deqp!) to trigger its mock-deqp mode

        let output = child
            .spawn()
            .with_context(|| format!("Spawning {:?}", deqp_runner))?
            .wait_with_output()
            .context("waiting for deqp-runner")?;

        let results_path = output_dir.path().to_owned().join("results.csv");
        let results = std::fs::File::open(&results_path)
            .with_context(|| format!("opening {:?}", &results_path))
            .and_then(|mut f| RunnerResults::from_csv(&mut f).context("reading results.csv"));

        output_dir.close().context("deleting temp output dir")?;

        Ok(RunnerCommandResult {
            status: output.status,
            stdout: String::from_utf8(output.stdout).context("UTF-8 of stdout")?,
            stderr: String::from_utf8(output.stderr).context("UTF-8 of stderr")?,
            results,
        })
    }

    pub fn with_baseline<S>(&mut self, baseline: S) -> &mut DeqpMock
    where
        S: AsRef<str>,
    {
        for line in baseline.as_ref().lines() {
            self.baseline.push(line.to_string());
        }
        self
    }

    pub fn with_skips<S>(&mut self, skips: S) -> &mut DeqpMock
    where
        S: AsRef<str>,
    {
        for line in skips.as_ref().lines() {
            self.skips.push(line.to_string());
        }
        self
    }

    pub fn with_flakes<S>(&mut self, flakes: S) -> &mut DeqpMock
    where
        S: AsRef<str>,
    {
        for line in flakes.as_ref().lines() {
            self.flakes.push(line.to_string());
        }
        self
    }

    pub fn with_runner_arg(&mut self, arg: &str) -> &mut DeqpMock {
        self.runner_args.push(arg.into());
        self
    }
}

fn mocked_deqp_runner<S: AsRef<str>>(tests: Vec<S>) -> RunnerResults {
    DeqpMock::new().run(tests).unwrap().results.unwrap()
}

fn result_status<S: AsRef<str>>(results: &RunnerResults, test: S) -> RunnerStatus {
    results.tests.get(test.as_ref()).unwrap().status
}

#[test]
fn many_passes() {
    let mut tests = Vec::new();
    for i in 0..1000 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }

    let result = DeqpMock::new().run(tests).unwrap();
    assert_eq!(result.stderr, "");
    assert_eq!(result.status.code(), Some(0));
    assert!(result.stdout.contains("Pass: 1000"));
    assert_eq!(result.results.unwrap().result_counts.pass, 1000);
}

#[test]
fn many_passes_and_a_fail() {
    let mut tests = Vec::new();
    for i in 0..1000 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    tests.push("dEQP-GLES2.test.f.foo".to_string());

    let results = mocked_deqp_runner(tests);
    assert_eq!(results.result_counts.pass, 1000);
    assert_eq!(results.result_counts.fail, 1);
    assert_eq!(
        result_status(&results, "dEQP-GLES2.test.f.foo"),
        RunnerStatus::Fail
    );
}

#[test]
fn crash() {
    let mut tests = Vec::new();
    for i in 0..50 {
        tests.push(format!("dEQP-GLES2.test.{}.p.foo", i));
    }

    tests.push("dEQP-GLES2.test.50.c.foo".to_string());

    for i in 51..100 {
        tests.push(format!("dEQP-GLES2.test.{}.p.foo", i));
    }

    let results = mocked_deqp_runner(tests);
    assert_eq!(results.result_counts.pass, 99);
    assert_eq!(results.result_counts.crash, 1);
    assert_eq!(
        result_status(&results, "dEQP-GLES2.test.50.c.foo"),
        RunnerStatus::Crash
    );
}

#[test]
fn timeout() {
    let mut tests = Vec::new();
    for i in 0..20 {
        if i % 7 == 1 {
            tests.push(format!("dEQP-GLES2.test.{}.timeout.foo", i));
        } else {
            tests.push(format!("dEQP-GLES2.test.{}.p.foo", i));
        }
    }

    let results = mocked_deqp_runner(tests);
    assert_eq!(results.result_counts.pass, 17);
    assert_eq!(results.result_counts.timeout, 3);
    assert_eq!(
        result_status(&results, "dEQP-GLES2.test.1.timeout.foo"),
        RunnerStatus::Timeout
    );
}

// Tests a run with a skips list like we might actually write in Mesa CI
#[test]
fn skip_crash() {
    let mut tests = Vec::new();
    for i in 0..100 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    tests.push("dEQP-GLES2.test.c.foo".to_string());

    let results = DeqpMock::new()
        .with_skips(
            "
# Skip all crashing tests

dEQP-GLES2.test.c.*
",
        )
        .run(tests)
        .unwrap()
        .results
        .unwrap();

    assert_eq!(results.result_counts.pass, 100);
    assert_eq!(results.result_counts.crash, 0);
    assert_eq!(results.result_counts.skip, 1);
    assert_eq!(
        result_status(&results, "dEQP-GLES2.test.c.foo"),
        RunnerStatus::Skip
    );
}

// Tests a run with a flakes list like we might actually write in Mesa CI
#[test]
fn flake_handling() {
    let mut tests = Vec::new();
    for i in 0..100 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    for i in 0..2 {
        tests.push(format!("dEQP-GLES2.test.flaky.{}", i));
    }

    {
        // Verify that our mocked flaky test actually flakes, and that
        // the default flake handling detects it!
        let mut found_pass = false;
        let mut found_fail = false;
        let mut found_flake = false;
        while !(found_fail && found_pass && found_flake) {
            let results = mocked_deqp_runner(tests.clone());
            match result_status(&results, "dEQP-GLES2.test.flaky.0") {
                RunnerStatus::Pass => found_pass = true,
                RunnerStatus::Fail => found_fail = true,
                RunnerStatus::Flake => found_flake = true,
                _ => unreachable!("bad test result"),
            }
        }
    }

    {
        // Verify that we can handle known flakes
        let mut found_flake = false;
        let mut found_pass = false;
        let mut found_xfail = false;
        while !(found_flake && found_pass && found_xfail) {
            let results = DeqpMock::new()
                .with_flakes("dEQP-GLES2.test.flaky.*\n")
                .with_baseline("dEQP-GLES2.test.flaky.1,Fail")
                .run(tests.clone())
                .unwrap()
                .results
                .unwrap();

            match result_status(&results, "dEQP-GLES2.test.flaky.0") {
                RunnerStatus::Pass => found_pass = true,
                RunnerStatus::Flake => {
                    found_flake = true;
                    assert!(results.result_counts.flake >= 1);
                }
                _ => unreachable!("bad test result"),
            }

            match result_status(&results, "dEQP-GLES2.test.flaky.1") {
                RunnerStatus::ExpectedFail => found_xfail = true,
                RunnerStatus::Flake => {
                    found_flake = true;
                    assert!(results.result_counts.flake >= 1);
                }
                _ => unreachable!("bad test result"),
            }
        }
    }
}

#[test]
fn baseline() {
    let mut tests = Vec::new();
    for i in 0..10 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    for i in 0..4 {
        tests.push(format!("dEQP-GLES2.test.f.{}", i));
    }
    for i in 0..2 {
        tests.push(format!("dEQP-GLES2.test.c.{}", i));
    }

    let results = DeqpMock::new()
        .with_baseline(
            "
dEQP-GLES2.test.p.1,Fail
dEQP-GLES2.test.f.2,Fail
dEQP-GLES2.test.f.3,Fail
dEQP-GLES2.test.c.1,Crash",
        )
        .run(tests)
        .unwrap()
        .results
        .unwrap();

    assert_eq!(results.result_counts.pass, 9);
    assert_eq!(results.result_counts.unexpected_pass, 1);
    assert_eq!(results.result_counts.crash, 1);
    assert_eq!(results.result_counts.fail, 2);
    assert_eq!(results.result_counts.expected_fail, 3);
}

#[test]
fn missing() {
    let mut tests = Vec::new();
    for i in 0..100 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    tests.push("dEQP-GLES2.test.m.foo".to_string());

    let results = mocked_deqp_runner(tests);
    assert_eq!(results.result_counts.pass, 100);
    assert_eq!(results.result_counts.missing, 1);
    assert_eq!(
        result_status(&results, "dEQP-GLES2.test.m.foo"),
        RunnerStatus::Missing
    );
}

// Tests round-tripping some results through csv formatting.
#[test]
fn results_serialization() {
    let mut tests = Vec::new();
    for i in 0..50 {
        tests.push(format!("dEQP-GLES2.test.p.{}", i));
    }
    for i in 0..30 {
        tests.push(format!("dEQP-GLES2.test.f.{}", i));
    }
    for i in 0..20 {
        tests.push(format!("dEQP-GLES2.test.s.{}", i));
    }
    for i in 0..10 {
        tests.push(format!("dEQP-GLES2.test.m.{}", i));
    }
    tests.push("dEQP-GLES2.test.c.foo".to_string());
    let results = mocked_deqp_runner(tests);

    let mut results_file = Cursor::new(Vec::new());
    results.write_results(&mut results_file).unwrap();
    results_file.set_position(0);
    let read_results = RunnerResults::from_csv(&mut results_file).unwrap();
    assert_eq!(results.result_counts, read_results.result_counts);

    let mut results_file = Cursor::new(Vec::new());
    results.write_failures(&mut results_file).unwrap();
    results_file.set_position(0);
    let read_results = RunnerResults::from_csv(&mut results_file).unwrap();
    assert_eq!(0, read_results.result_counts.pass);
    assert_eq!(0, read_results.result_counts.skip);
    assert_eq!(results.result_counts.fail, read_results.result_counts.fail);
    assert_eq!(
        results.result_counts.crash,
        read_results.result_counts.crash
    );
}

#[test]
fn missing_skips() {
    let results = DeqpMock::new()
        .with_runner_arg("--skips")
        .with_runner_arg("/does-not-exist.txt")
        .run(vec!["dEQP-GLES2.test.p.1"])
        .unwrap();
    assert_eq!(Some(1), results.status.code());
    println!("{}", results.stderr);
}

#[test]
fn missing_flakes() {
    let results = DeqpMock::new()
        .with_runner_arg("--flakes")
        .with_runner_arg("/does-not-exist.txt")
        .run(vec!["dEQP-GLES2.test.p.1"])
        .unwrap();
    assert_eq!(Some(1), results.status.code());
    println!("{}", results.stderr);
}

#[test]
fn includes() {
    let results = DeqpMock::new()
        .with_runner_arg("-t")
        .with_runner_arg("dEQP-GLES2.test.p.*")
        .run(vec!["dEQP-GLES2.test.p.1", "dEQP-GLES2.test.f.2"])
        .unwrap()
        .results
        .unwrap();
    assert_eq!(results.result_counts.pass, 1);
    assert_eq!(results.result_counts.fail, 0);
    assert_eq!(results.result_counts.total, 1);
}

#[test]
fn bad_includes() {
    let results = DeqpMock::new()
        .with_runner_arg("-t")
        .with_runner_arg("*")
        .run(vec!["dEQP-GLES2.test.p"])
        .unwrap();
    assert_eq!(Some(1), results.status.code());
}
