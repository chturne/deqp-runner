use ::deqp_runner::RunnerResults;
use anyhow::{Context, Result};
use std::io::prelude::*;

/// Integration test binary.  See https://matklad.github.io/2021/02/27/delete-cargo-integration-tests.html
mod deqp_runner;
mod piglit_runner;

// All the output we capture from an invocation of deqp-runner
struct RunnerCommandResult {
    status: std::process::ExitStatus,
    stdout: String,
    stderr: String,

    results: Result<RunnerResults>,
}

pub fn lines_tempfile<S: AsRef<str>, I: IntoIterator<Item = S>>(
    lines: I,
) -> Result<tempfile::TempPath> {
    let mut file = tempfile::NamedTempFile::new().context("creating tempfile")?;
    for line in lines {
        writeln!(file, "{}", line.as_ref()).context("writing tempfile")?;
    }
    Ok(file.into_temp_path())
}
