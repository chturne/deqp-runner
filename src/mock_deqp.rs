use anyhow::{Context, Result};
use rand::Rng;
use std::io::{BufRead, BufReader};
use std::{fs::File, path::PathBuf};
use structopt::StructOpt;

/// Mock deqp that uses conventions in the test name to control behavior of the
/// test.  We use this for integration testing of deqp-runner.

#[derive(Debug, StructOpt)]
pub struct MockDeqp {
    #[structopt(long, help = "Path to the caselist file")]
    deqp_caselist_file: PathBuf,

    #[structopt(long, help = "Path to the QPA log output")]
    deqp_log_filename: PathBuf,

    #[structopt(long)]
    deqp_log_flush: bool,

    #[structopt(long, help = "Path to the .shader_cache output")]
    deqp_shadercache_filename: PathBuf,

    #[structopt(long)]
    deqp_shadercache_truncate: bool,
}

pub fn mock_deqp(mock: &MockDeqp) -> Result<()> {
    let file = File::open(&mock.deqp_caselist_file)
        .with_context(|| format!("Opening {:?}", &mock.deqp_caselist_file))?;

    let tests = BufReader::new(file)
        .lines()
        .collect::<std::result::Result<Vec<String>, std::io::Error>>()
        .context("reading test caselist")?;

    File::create(&mock.deqp_log_filename).context("Creating QPA file")?;

    for test_name in tests {
        // Missing tests won't appear in the output at all.
        if test_name.contains("dEQP-GLES2.test.m.") {
            continue;
        }

        println!("Test case '{}'..", test_name);

        if test_name.contains(".timeout.") {
            // Simulate a testcase that doesn't return in time by infinite
            // looping.
            #[allow(clippy::empty_loop)]
            loop {}
        }

        if test_name.contains(".p.") {
            println!("  Pass (success case)");
        } else if test_name.contains(".f.") {
            println!("  Fail (failure case)");
        } else if test_name.contains(".flaky") {
            if rand::thread_rng().gen::<bool>() {
                println!("  Fail (failure case)");
            } else {
                println!("  Pass (success)");
            }
        } else if test_name.contains(".s.") {
            println!("  NotSupported (skip case)");
        } else if test_name.contains(".c.") {
            // In a crash, the output just stops before we get a result and
            // the process returns an error code.  parse_deqp_results() just
            // handles the deqp output unexpectedly ending as a crash.
            break;
        } else {
            unimplemented!("unknown mock test name {}", test_name)
        }
    }

    Ok(())
}
